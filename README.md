# PyMX Septiembre 2023

## Usando Servicios Administrados de AI de AWS con Python y Boto3

![PyMX](images/pymx.png)

El Jupyter Notebook de la charla es [pymx20230912.ipynb](pymx20230912.ipynb).

[pymxpymx20230912_final.ipynb](pymx20230912_final.ipynb) es el Notebook ya "ejecutado", en el caso de que solo quieras ver el resultado final.

## Requirementos para ejecutar

- Python 3.8+
- Poetry
- Una cuenta de AWS con permisos para:
  - Comprehend
  - Polly
  - Rekognition
  - Transcribe
  - Translate

Nota: La capa gratuita de AWS soporta todas las llamadas sin que se tenga que hacer algún pago.

## Instrucciones

Ejecutar en la consola:

```bash
poetry install
poetry shell
jupyter notebook pymx20230912.ipynb
```

## Abrir en [Binder](https://mybinder.org)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/soldavid%2Fpymx20230912/main?labpath=pymx20230912.ipynb)

## Thank you very much
